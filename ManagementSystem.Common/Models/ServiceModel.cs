﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ManagementSystem.Common.Models
{
    public class ServiceModel
    {
        public ServiceModel(bool accountValid, AccountRole accountRole)
        {
            this.AccountValid = accountValid;
            this.AccountRole = accountRole;
        }

        public bool AccountValid { get; private set; }
        public AccountRole AccountRole { get; private set; }
        public AccountState AccountState { get; set; }
    }

    public enum AccountState
    {
        Active,
        Deactivated,
        Idle,
        Deleted,
        NotFound
    }

    public enum AccountRole
    {
        ACCOUNT_ADMIN,
        ACCOUNT_VENDOR,
        ACCOUNT_CUSTOMER,
        ACCOUNT_GUEST,
        ACCOUNT_INVALID
    }
}
