﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ManagementSystem.Common.Models;
using ManagementSystem.DataContext.Models;

namespace ManagementSystem.Common.Contracts
{
    public interface IAccountService : IDisposable
    {
        Task<int> InsertAccountInformationAsync(User accountInformation, AccountRole accountRole = AccountRole.ACCOUNT_CUSTOMER);
    }
}
