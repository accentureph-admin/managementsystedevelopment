﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ManagementSystem.Common.Contracts
{
    public interface IAccountSecurity : IDisposable
    {
        Task<string> Encrypt(string password);
        Task<string> Decrypt(string password);
    }
}
