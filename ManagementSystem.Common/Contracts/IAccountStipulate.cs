﻿using ManagementSystem.Common.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ManagementSystem.Common.Contracts
{
    public interface IAccountStipulate : IDisposable
    {
        Task<int> CheckAccountInformationAsync(string username);
        Task<ServiceModel> GetAccountInformationAsync(string username, string password);
    }
}
