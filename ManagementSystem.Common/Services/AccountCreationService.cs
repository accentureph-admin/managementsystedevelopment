﻿using ManagementSystem.DataContext.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ManagementSystem.Common.Models;

namespace ManagementSystem.Common.Services
{
    public class AccountCreationService
    {
        private readonly AccountSecurity _accountSecurity;
        private readonly User _accountInformation;
        public AccountCreationService(User accountInformation, AccountRole accountrole = AccountRole.ACCOUNT_CUSTOMER)
        {
            AccountRole = accountrole;
            _accountSecurity = new AccountSecurity();
            _accountInformation = accountInformation;
        }

        public string Id { get; private set; } = Guid.NewGuid().ToString();
        public string Password { get; private set; }
        public AccountRole AccountRole { get; private set; }
        public string IsActitve { get; private set; } = "0";


        public async Task<User> AccountInformation() {

            switch (AccountRole)
            {
                case AccountRole.ACCOUNT_ADMIN:
                    AccountRole = AccountRole.ACCOUNT_ADMIN;
                    break;
                case AccountRole.ACCOUNT_GUEST:
                    AccountRole = AccountRole.ACCOUNT_GUEST;
                    break;
                case AccountRole.ACCOUNT_INVALID:
                    AccountRole = AccountRole.ACCOUNT_INVALID;
                    break;
                case AccountRole.ACCOUNT_VENDOR:
                    AccountRole = AccountRole.ACCOUNT_VENDOR;
                    break;
                default:
                    AccountRole = AccountRole.ACCOUNT_CUSTOMER;
                    break;
            }

            return new User(){
                Id = Id,
                Username = _accountInformation.Username,
                Password = await _accountSecurity.Encrypt(_accountInformation.Password),
                Role = AccountRole.ToString(),
                Isactive = IsActitve
            };
        }
    }
}
