﻿using ManagementSystem.Common.Contracts;
using ManagementSystem.Common.Models;
using ManagementSystem.DataContext.DBDataContext;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagementSystem.Common.Services
{
    public class AccountStipulate : IAccountStipulate
    {
        private readonly ManagementDatabaseContext _context;
        private readonly AccountSecurity _accountSecurity;
        private bool disposedValue;

        public AccountStipulate(ManagementDatabaseContext context)
        {
            _context = context;
            _accountSecurity = new AccountSecurity();
        }
        public async Task<ServiceModel> GetAccountInformationAsync(string username, string password)
        {

            var encryptedPassword = await _accountSecurity.Encrypt(password);
            var accountInformation = await _context.Users
                .Where(param => param.Username
                .Equals(username) && param.Password
                .Equals(encryptedPassword))
                .FirstOrDefaultAsync();

            var accountService = accountInformation != null
                                 ? new ServiceModel(accountValid: true, accountRole: (AccountRole)Enum.Parse(typeof(AccountRole), accountInformation.Role)) { AccountState = (AccountState)Convert.ToInt32(accountInformation.Isactive) }
                                 : new ServiceModel(accountValid: false, accountRole: AccountRole.ACCOUNT_INVALID);

            return accountService;
        }

        public async Task<int> CheckAccountInformationAsync(string username)
        {
            return await _context.Users
                .Where(param => param.Username
                .Equals(username)).CountAsync();
        } 

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects)
                }

                // TODO: free unmanaged resources (unmanaged objects) and override finalizer
                // TODO: set large fields to null
                disposedValue = true;
            }
        }

        // // TODO: override finalizer only if 'Dispose(bool disposing)' has code to free unmanaged resources
        // ~AccountStipulate()
        // {
        //     // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method

            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
