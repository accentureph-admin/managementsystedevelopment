﻿using System;
using System.Collections.Generic;
using System.Text;
using ManagementSystem.DataContext.DBDataContext;
using ManagementSystem.DataContext.Models;
using ManagementSystem.Common.Contracts;
using ManagementSystem.Common.Models;
using ManagementSystem.Common.Services;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ManagementSystem.Common.Services
{
    public class AccountService : IAccountService
    {
        private readonly ManagementDatabaseContext _context;
        private readonly AccountSecurity _accountSecurity;
        private User _accountCreation;
        private AccountCreationService _accountCreationService;
        public AccountService(ManagementDatabaseContext context) {
            _context = context;
            _accountSecurity = new AccountSecurity();
            _accountCreation = new User();
        }

        public async Task<int> InsertAccountInformationAsync(User accountInformation, AccountRole accountRole = AccountRole.ACCOUNT_CUSTOMER) {
            
            _accountCreationService = new AccountCreationService(accountInformation, accountRole);
            _accountCreation = await _accountCreationService.AccountInformation();
            _context.Attach(_accountCreation);
            _context.Entry(_accountCreation).State = EntityState.Added;
            
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
