﻿using AspNetCoreHero.ToastNotification.Abstractions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ManagementSystem.DataContext.DBDataContext;
using ManagementSystem.Common.Contracts;
using ManagementSystem.Common.Services;
using ManagementSystem.Common.Models;
using ManagementSystem.DataContext.DataContract;
using ManagementSystem.Models;
using Microsoft.AspNetCore.Session;
using Microsoft.AspNetCore.Http;
using ManagementSystem.DataContext.Models;

namespace ManagementSystem.Controllers
{
    [Route("login")]
    public class LoginController : Controller
    {
        private readonly INotyfService _notyf;
        private readonly IManagementDatabase _context;
        private readonly IAccountService _accountServices;
        private readonly IAccountStipulate _accountAuthentication;
        public LoginController(INotyfService notyf
            , IManagementDatabase context
            , IAccountService accountService
            , IAccountStipulate accountAuthentication)
        {
            _context = context;
            _notyf = notyf;
            _accountServices = accountService;
            _accountAuthentication = accountAuthentication;
        }

        [Route("")]
        [Route("index")]
        [Route("~/")]
        public IActionResult Index()
        {
            return View();
        }
        [Route("login")]
        [HttpPost]
        public async Task<IActionResult> Login(string username, string password)
        {
            var accountAuth = await _accountAuthentication.GetAccountInformationAsync(username: username, password: password);
            if (accountAuth.AccountValid)
            {
                if (accountAuth.AccountState == AccountState.Active)
                {
                    HttpContext.Session.SetString(SessionConstants.USER_CONTEXT, username);
                    HttpContext.Session.SetString(SessionConstants.USER_ROLE, Convert.ToString(accountAuth.AccountRole));
                    HttpContext.Session.SetString(SessionConstants.USER_TOKEN, "kjhN89Nasda");
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    _notyf.Warning("Account Deactivated, contact administrator for further actions");
                    return View("index");
                }
            }
            else
            {
                _notyf.Warning("Invalid Credintials");
                return View("index");
            }
        }

        [Route("register")]
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }


        [Route("register")]
        [HttpPost]
        public async Task<IActionResult> Register(User accountInformation)
        {
            var accountAuthentication = await _accountAuthentication.CheckAccountInformationAsync(accountInformation.Username);
            if (accountAuthentication > 0)
            {
                _notyf.Warning("Registration Failed, Email already registered");
                return View("register");
            }
            else
            {
                var accountService = await _accountServices.InsertAccountInformationAsync(accountInformation, AccountRole.ACCOUNT_CUSTOMER) ;
                if (accountService > 0)
                {
                    _notyf.Success("Registration Successful");
                    return View("index");
                }
                else
                {
                    _notyf.Warning("Registration Failed, Contact administrator for Actions");
                    return View("register");
                }
            }
        }
    }
}
