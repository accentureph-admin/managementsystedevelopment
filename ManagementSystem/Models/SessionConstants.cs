﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManagementSystem.Models
{
    public class SessionConstants
    {
        public const string USER_CONTEXT = "Userinfo";
        public const string USER_ROLE = "UserRole";
        public const string USER_TOKEN = "AuthToken";
    }
}
