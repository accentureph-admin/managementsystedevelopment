﻿using ManagementSystem.DataContext.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ManagementSystem.DataContext.DataContract
{
    public interface IManagementDatabase
    {
        DbSet<Category> Categories { get; set; }
        DbSet<Customer> Customers { get; set; }
        DbSet<Product> Products { get; set; }
        DbSet<User> Users { get; set; }
    }
}
