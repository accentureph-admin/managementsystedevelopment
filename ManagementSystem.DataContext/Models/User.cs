﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace ManagementSystem.DataContext.Models
{
    public partial class User
    {
        [Key]
        [StringLength(50)]
        public string Id { get; set; }
        [StringLength(50)]
        public string Username { get; set; }
        [StringLength(50)]
        public string Role { get; set; }
        [StringLength(8, ErrorMessage = "Name length can't be more than 8.")]
        public string Password { get; set; }
        [StringLength(50)]
        public string Isactive { get; set; }
    }
}
