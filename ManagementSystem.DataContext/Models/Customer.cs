﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace ManagementSystem.DataContext.Models
{
    public partial class Customer
    {
        [Key]
        [StringLength(50)]
        public string CustomerId { get; set; }
        [StringLength(50)]
        public string FirstName { get; set; }
        [StringLength(50)]
        public string LastName { get; set; }
        [StringLength(500)]
        public string PhotoUrl { get; set; }
        [StringLength(50)]
        public string PhoneNo { get; set; }
        [Required]
        [StringLength(50)]
        public string EmailId { get; set; }
    }
}
